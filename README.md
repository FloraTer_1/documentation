# **FloraTer_1**

# Opis projektu

FloraTer_1 to aplikacja ułatwiająca i przyśpieszająca proces opisywania roślin. Aplikacja ma za zadanie zbieranie oraz przechowywanie danych terenowych roślin celem późniejszej ich analizy.

# Członkowie grupy
  
Artur Białoskórski  
Krzysztof Stępniak  
Mikołaj Celka  
Sebastian Ufnal  

# Dokumenty

## Dokumenty wstępne  

### Wymagania

https://eduwiki.wmi.amu.edu.pl/chris/DPRI1_2019

### Propozycja tematu projektu

https://bit.ly/2Kc9KHU

### Value Proposition Canvas

https://bit.ly/2Ulaejm

### Wizja projektu

https://bit.ly/2ORgFoA

### User Stories

https://bit.ly/2FPneoA

## Dokementy na pierwszy przyrost  

### Prezentacja  

https://docs.google.com/presentation/d/1C4Xjf3-2GYueRh7pJzgMfZUz-wieOiK9xKy89g9P7iE

### Zakres projektu

https://docs.google.com/document/d/1hEevaLne9HizoO41Tx6ZMMsET-FxM3B4Vc-fnLlraK8

### Specyfikacja wymagań  
  
https://docs.google.com/document/d/1JkC_R_0cRmw_dLe4_Zsht8QtrjsgrxUMvhS8t4o44n0 

## Link do dysku google

https://bit.ly/2UDoWSc